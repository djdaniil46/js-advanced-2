"use strict";
const root = document.getElementById("root");
const books = [
    { 
      author: "Скотт Бэккер",
      name: "Тьма, что приходит прежде",
      price: 70 
    }, 
    {
     author: "Скотт Бэккер",
     name: "Воин-пророк",
    }, 
    { 
      name: "Тысячекратная мысль",
      price: 70
    }, 
    { 
      author: "Скотт Бэккер",
      name: "Нечестивый Консульт",
      price: 70
    }, 
    {
     author: "Дарья Донцова",
     name: "Детектив на диете",
     price: 40
    },
    {
     author: "Дарья Донцова",
     name: "Дед Снегур и Морозочка",
    }
  ];

class Book{
  constructor(author, name, price){
    this.author = author;
    this.name = name;
    this.price = price;
  }
  createList(){
    const ul = document.createElement("ul");
    for (const key in this) {
      if (Object.hasOwnProperty.call(this, key)) {
        const item = this[key];
        let step = 0;
        const li = document.createElement("li");
        li.textContent = item;
        ul.append(li);
        step+=1;
        if(step=3){
          root.append(ul);
        }
      }
    }
  }
}

try {
  for (let index = 0; index < books.length; index++) {
    const object = new Book(books[index].name, books[index].author, books[index].price);
    if(object.name!==undefined&&object.author!==undefined&&object.price!==undefined){
      object.createList();
    }
    else if(object.name===undefined||!object.name){
      throw new SyntaxError("Wrong input, name cannot be undefined");
    }
    else if(object.price===undefined||!object.price){
      throw new SyntaxError("Wrong input, price cannot be undefined");
    }
    else if(object.author===undefined||!object.author){
      throw new SyntaxError("Wrong input, author cannot be undefined");
    }
  }
} catch (error) {
  console.error("Извините, в данных ошибка | "+error);
}